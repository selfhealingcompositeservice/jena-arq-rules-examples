package main;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.reasoner.rulesys.GenericRuleReasoner;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.rulesys.Rule;
import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.update.UpdateAction;
import com.hp.hpl.jena.update.UpdateExecutionFactory;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateProcessor;
import com.hp.hpl.jena.update.UpdateRequest;

import java.net.MalformedURLException;

public class ReasonWithRules {

	/**
	 * @param args
	 * @throws MalformedURLException
	 *             http://shortmemory2012.blogspot.fr/2012/11/jena-tutorial-
	 *             reasoning-with-user.html
	 */
	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub

		Model model = ModelFactory.createDefaultModel();
		//instances.read("file:dataset.txt", "N3");
		Resource r = model.createResource("http://jena.hpl.hp.com/prefix#time");
		Property p = model
				.createProperty("http://www.w3.org/2001/XMLSchema#integervalue");
		
		model.add(r, p, model.createTypedLiteral(19));
		Reasoner reasoner = new GenericRuleReasoner(
				Rule.rulesFromURL("file:rule.txt"));
		reasoner.setDerivationLogging(true);
		InfModel inf = ModelFactory.createInfModel(reasoner, model);

		printStatements(inf);
		query(inf);

		//update
	
		
		updateTime(model, 1);
		inf = ModelFactory.createInfModel(reasoner, model);
		
		System.out.println("statements after update");
		
		printStatements(inf);
		
		updateTime(model, 20);
		inf = ModelFactory.createInfModel(reasoner, model);
		
		System.out.println("statements after update");
		
		printStatements(inf);
		
	}

	static void printStatements(InfModel inf) {
		// print out the statements in the model
		StmtIterator iter = inf.listStatements();
		while (iter.hasNext()) {
			Statement stmt = iter.nextStatement();
			Resource subject = stmt.getSubject();
			Property predicate = stmt.getPredicate();
			RDFNode object = stmt.getObject();

			System.out.print("subject: " + subject.getLocalName());
			System.out.print(" -- predicate " + predicate.toString() + " ");
			if (object instanceof Resource) {
				System.out.print(" -- object instanceof Resource: "
						+ object.toString());
			} else {
				// object is a literal
				System.out.print(" -- object \"" + object.toString() + "\"");
			}
			System.out.println(" .");

		}
	}

	static void query(InfModel inf) {
		System.out.println("SPARQL");

		String sparql = "PREFIX integer: <http://www.w3.org/2001/XMLSchema#int>"
				+ "PREFIX : <http://jena.hpl.hp.com/prefix#>"
				+ "SELECT ?action  WHERE {<http://somewhere/Action/> :action ?action }";

		Query qry = QueryFactory.create(sparql);
		QueryExecution qe = QueryExecutionFactory.create(qry, inf);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution sol = rs.nextSolution();
			RDFNode str = sol.get("str");
			RDFNode thing = sol.get("thing");
			System.out.println("Resultet " + sol);
			System.out.println(sol.get("?action"));
		}

		qe.close();
	}
	
	static void updateTime(Model model, int time) {
		System.out.println("SPARQL UPDATE");

		String sparql = "PREFIX integer: <http://www.w3.org/2001/XMLSchema#int>"
				+ "PREFIX : <http://jena.hpl.hp.com/prefix#> "
				+ "DELETE {?s ?p ?o}  WHERE { ?s ?p ?o . FILTER (?s = :time) }";
		
		UpdateRequest queryObj=UpdateFactory.create(sparql);
	    
	    UpdateAction.execute(queryObj, model);
	    

		Resource r = model.createResource("http://jena.hpl.hp.com/prefix#time");
		Property p = model
				.createProperty("http://www.w3.org/2001/XMLSchema#integervalue");
		
		model.add(r, p, model.createTypedLiteral(time));

	}
}